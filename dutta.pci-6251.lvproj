<?xml version='1.0'?>
<Project Type="Project" LVVersion="8208000">
   <Item Name="My Computer" Type="My Computer">
      <Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.tcp.enabled" Type="Bool">false</Property>
      <Property Name="server.tcp.port" Type="Int">0</Property>
      <Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
      <Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="subVI" Type="Folder">
         <Item Name="Config Read.vi" Type="VI" URL="subVI/Config Read.vi"/>
         <Item Name="Config Write.vi" Type="VI" URL="subVI/Config Write.vi"/>
         <Item Name="Current Select.vi" Type="VI" URL="subVI/Current Select.vi"/>
         <Item Name="Doubles to Waveform.vi" Type="VI" URL="subVI/Doubles to Waveform.vi"/>
         <Item Name="GCF.vi" Type="VI" URL="subVI/GCF.vi"/>
         <Item Name="Get Channels_10 channels.vi" Type="VI" URL="subVI/Get Channels_10 channels.vi"/>
         <Item Name="Get Synchronized Waveforms.vi" Type="VI" URL="subVI/Get Synchronized Waveforms.vi"/>
         <Item Name="Load Data.vi" Type="VI" URL="subVI/Load Data.vi"/>
         <Item Name="Save Data.vi" Type="VI" URL="subVI/Save Data.vi"/>
         <Item Name="Separate Data.vi" Type="VI" URL="subVI/Separate Data.vi"/>
         <Item Name="Voltage Adjust.vi" Type="VI" URL="subVI/Voltage Adjust.vi"/>
         <Item Name="Waveform Adjust.vi" Type="VI" URL="subVI/Waveform Adjust.vi"/>
         <Item Name="header.vi" Type="VI" URL="subVI/header.vi"/>
         <Item Name="state.ctl" Type="VI" URL="subVI/state.ctl"/>
         <Item Name="filename auto-generate.vi" Type="VI" URL="subVI/filename auto-generate.vi"/>
         <Item Name="channel dialog.vi" Type="VI" URL="subVI/channel dialog.vi"/>
      </Item>
      <Item Name="single channel acquisition.vi" Type="VI" URL="single channel acquisition.vi"/>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build">
         <Item Name="Dutta Single-Channel Acq" Type="EXE">
            <Property Name="Absolute[0]" Type="Bool">true</Property>
            <Property Name="Absolute[1]" Type="Bool">true</Property>
            <Property Name="Absolute[2]" Type="Bool">true</Property>
            <Property Name="ActiveXInfoEnumCLSIDsItemCount" Type="Int">0</Property>
            <Property Name="ActiveXInfoMajorVersion" Type="Int">0</Property>
            <Property Name="ActiveXInfoMinorVersion" Type="Int">0</Property>
            <Property Name="ActiveXInfoObjCLSIDsItemCount" Type="Int">0</Property>
            <Property Name="ActiveXInfoProgIDPrefix" Type="Str"></Property>
            <Property Name="ActiveXServerName" Type="Str"></Property>
            <Property Name="AliasID" Type="Str">{9837BAA6-5738-41C5-BEE0-719CCA480490}</Property>
            <Property Name="AliasName" Type="Str">Project.aliases</Property>
            <Property Name="ApplicationID" Type="Str">{2CC57508-47FA-4C26-9E37-26ED9CF7444B}</Property>
            <Property Name="ApplicationName" Type="Str">SingleChannel.exe</Property>
            <Property Name="AutoIncrement" Type="Bool">false</Property>
            <Property Name="BuildName" Type="Str">Dutta Single-Channel Acq</Property>
            <Property Name="CommandLineArguments" Type="Bool">false</Property>
            <Property Name="CopyErrors" Type="Bool">false</Property>
            <Property Name="DebuggingEXE" Type="Bool">false</Property>
            <Property Name="DebugServerWaitOnLaunch" Type="Bool">false</Property>
            <Property Name="DefaultLanguage" Type="Str">English</Property>
            <Property Name="DependencyApplyDestination" Type="Bool">true</Property>
            <Property Name="DependencyApplyInclusion" Type="Bool">true</Property>
            <Property Name="DependencyApplyProperties" Type="Bool">true</Property>
            <Property Name="DependencyFolderDestination" Type="Int">0</Property>
            <Property Name="DependencyFolderInclusion" Type="Str">As needed</Property>
            <Property Name="DependencyFolderPropertiesItemCount" Type="Int">0</Property>
            <Property Name="DestinationID[0]" Type="Str">{3F7227D0-BE4C-4678-9A6B-CC07740B6220}</Property>
            <Property Name="DestinationID[1]" Type="Str">{3F7227D0-BE4C-4678-9A6B-CC07740B6220}</Property>
            <Property Name="DestinationID[2]" Type="Str">{7F4717A9-69EE-41AE-8808-BEBFB5C7918D}</Property>
            <Property Name="DestinationItemCount" Type="Int">3</Property>
            <Property Name="DestinationName[0]" Type="Str">SingleChannel.exe</Property>
            <Property Name="DestinationName[1]" Type="Str">Destination Directory</Property>
            <Property Name="DestinationName[2]" Type="Str">Support Directory</Property>
            <Property Name="Disconnect" Type="Bool">true</Property>
            <Property Name="IncludeHWConfig" Type="Bool">false</Property>
            <Property Name="IncludeSCC" Type="Bool">true</Property>
            <Property Name="INIID" Type="Str">{B3AA75AB-4292-4323-B85A-ADD20467D61F}</Property>
            <Property Name="ININame" Type="Str">LabVIEW.ini</Property>
            <Property Name="LOGID" Type="Str">{0681C6D1-E91D-4C72-ADD1-80A5F9A37AFE}</Property>
            <Property Name="MathScript" Type="Bool">false</Property>
            <Property Name="Path[0]" Type="Path">/Z/SW Distrib/Dutta/internal.llb</Property>
            <Property Name="Path[1]" Type="Path">/Z/SW Distrib/Dutta</Property>
            <Property Name="Path[2]" Type="Path">/Z/SW Distrib/Dutta/data</Property>
            <Property Name="ShowHWConfig" Type="Bool">false</Property>
            <Property Name="SourceInfoItemCount" Type="Int">16</Property>
            <Property Name="SourceItem[0].Destination" Type="Int">0</Property>
            <Property Name="SourceItem[0].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[0].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[0].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[0].IsFolder" Type="Bool">true</Property>
            <Property Name="SourceItem[0].ItemID" Type="Ref">/My Computer/subVI</Property>
            <Property Name="SourceItem[1].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[1].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[1].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[1].ItemID" Type="Ref">/My Computer/subVI/Config Read.vi</Property>
            <Property Name="SourceItem[1].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[1].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[1].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[10].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[10].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[10].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[10].ItemID" Type="Ref">/My Computer/subVI/Separate Data.vi</Property>
            <Property Name="SourceItem[10].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[10].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[10].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[11].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[11].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[11].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[11].ItemID" Type="Ref">/My Computer/subVI/Voltage Adjust.vi</Property>
            <Property Name="SourceItem[11].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[11].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[11].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[12].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[12].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[12].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[12].ItemID" Type="Ref">/My Computer/subVI/Waveform Adjust.vi</Property>
            <Property Name="SourceItem[12].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[12].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[12].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[13].Inclusion" Type="Str">Startup VI</Property>
            <Property Name="SourceItem[13].ItemID" Type="Ref">/My Computer/single channel acquisition.vi</Property>
            <Property Name="SourceItem[13].VIPropertiesItemCount" Type="Int">2</Property>
            <Property Name="SourceItem[13].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[13].VIPropertiesSettingOption[1]" Type="Str">Run when opened</Property>
            <Property Name="SourceItem[13].VIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[13].VIPropertiesVISetting[1]" Type="Bool">true</Property>
            <Property Name="SourceItem[14].ItemID" Type="Ref">/My Computer/subVI/filename auto-generate.vi</Property>
            <Property Name="SourceItem[14].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[14].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[14].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[15].ItemID" Type="Ref">/My Computer/subVI/header.vi</Property>
            <Property Name="SourceItem[15].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[15].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[15].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[2].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[2].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[2].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[2].ItemID" Type="Ref">/My Computer/subVI/Config Write.vi</Property>
            <Property Name="SourceItem[2].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[2].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[2].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[3].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[3].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[3].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[3].ItemID" Type="Ref">/My Computer/subVI/Current Select.vi</Property>
            <Property Name="SourceItem[3].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[3].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[3].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[4].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[4].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[4].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[4].ItemID" Type="Ref">/My Computer/subVI/Doubles to Waveform.vi</Property>
            <Property Name="SourceItem[4].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[4].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[4].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[5].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[5].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[5].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[5].ItemID" Type="Ref">/My Computer/subVI/GCF.vi</Property>
            <Property Name="SourceItem[5].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[5].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[5].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[6].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[6].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[6].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[6].ItemID" Type="Ref">/My Computer/subVI/Get Channels_10 channels.vi</Property>
            <Property Name="SourceItem[6].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[6].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[6].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[7].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[7].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[7].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[7].ItemID" Type="Ref">/My Computer/subVI/Get Synchronized Waveforms.vi</Property>
            <Property Name="SourceItem[7].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[7].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[7].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[8].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[8].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[8].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[8].ItemID" Type="Ref">/My Computer/subVI/Load Data.vi</Property>
            <Property Name="SourceItem[8].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[8].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[8].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="SourceItem[9].FolderPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[9].FolderVIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[9].FolderVIPropertiesVISetting[0]" Type="Bool">false</Property>
            <Property Name="SourceItem[9].ItemID" Type="Ref">/My Computer/subVI/Save Data.vi</Property>
            <Property Name="SourceItem[9].VIPropertiesItemCount" Type="Int">1</Property>
            <Property Name="SourceItem[9].VIPropertiesSettingOption[0]" Type="Str">Remove panel</Property>
            <Property Name="SourceItem[9].VIPropertiesVISetting[0]" Type="Bool">true</Property>
            <Property Name="StripLib" Type="Bool">true</Property>
            <Property Name="SupportedLanguageCount" Type="Int">0</Property>
            <Property Name="TLBID" Type="Str">{2CA71655-D7BF-4300-B412-26875E2514E6}</Property>
            <Property Name="UseFFRTE" Type="Bool">false</Property>
            <Property Name="VersionInfoCompanyName" Type="Str">University of Wyoming</Property>
            <Property Name="VersionInfoFileDescription" Type="Str"></Property>
            <Property Name="VersionInfoFileType" Type="Int">1</Property>
            <Property Name="VersionInfoFileVersionBuild" Type="Int">0</Property>
            <Property Name="VersionInfoFileVersionMajor" Type="Int">1</Property>
            <Property Name="VersionInfoFileVersionMinor" Type="Int">0</Property>
            <Property Name="VersionInfoFileVersionPatch" Type="Int">0</Property>
            <Property Name="VersionInfoInternalName" Type="Str">My Application</Property>
            <Property Name="VersionInfoLegalCopyright" Type="Str">Copyright © 2019 University of Wyoming</Property>
            <Property Name="VersionInfoProductName" Type="Str">Single Channel Acq</Property>
         </Item>
         <Item Name="My Installer" Type="Installer">
            <Property Name="Absolute" Type="Bool">true</Property>
            <Property Name="arpCompany" Type="Str">University of Wyoming</Property>
            <Property Name="arpContact" Type="Str">Marvin Perry</Property>
            <Property Name="arpPhone" Type="Str">+1 307 760 7554</Property>
            <Property Name="arpURL" Type="Str">http://www.uwyo.edu</Property>
            <Property Name="AutoIncrement" Type="Bool">true</Property>
            <Property Name="BuildLabel" Type="Str">My Installer</Property>
            <Property Name="BuildLocation" Type="Path">/Z/SW Distrib/Dutta</Property>
            <Property Name="DirInfo.Count" Type="Int">1</Property>
            <Property Name="DirInfo.DefaultDir" Type="Str">{8323CD25-0B33-4728-8A03-A9E27956561C}</Property>
            <Property Name="DirInfo[0].DirName" Type="Str">dutta.pci-6251</Property>
            <Property Name="DirInfo[0].DirTag" Type="Str">{8323CD25-0B33-4728-8A03-A9E27956561C}</Property>
            <Property Name="DirInfo[0].ParentTag" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
            <Property Name="DistID" Type="Str">{E4FE26F9-2E05-47BB-BE48-D3B70D5F86B0}</Property>
            <Property Name="DistParts.Count" Type="Int">2</Property>
            <Property Name="DistPartsInfo[0].FlavorID" Type="Str">DefaultFull</Property>
            <Property Name="DistPartsInfo[0].ProductID" Type="Str">{45FA54F6-8574-49D2-9E2D-0BDDE6237822}</Property>
            <Property Name="DistPartsInfo[0].ProductName" Type="Str">NI LabVIEW Run-Time Engine 8.2.1</Property>
            <Property Name="DistPartsInfo[0].UpgradeCode" Type="Str">{B5171839-26E3-48D9-9FD6-AF7F39055146}</Property>
            <Property Name="DistPartsInfo[1].FlavorID" Type="Str">_full_</Property>
            <Property Name="DistPartsInfo[1].ProductID" Type="Str">{3B6787F9-9752-4140-977F-EE2402EEB3F8}</Property>
            <Property Name="DistPartsInfo[1].ProductName" Type="Str">NI-DAQmx MAX Configuration Support 9.0</Property>
            <Property Name="DistPartsInfo[1].UpgradeCode" Type="Str">{9856368A-ED47-4944-87BE-8EF3472AE39B}</Property>
            <Property Name="FileInfo.Count" Type="Int">3</Property>
            <Property Name="FileInfo[0].DirTag" Type="Str">{8323CD25-0B33-4728-8A03-A9E27956561C}</Property>
            <Property Name="FileInfo[0].FileName" Type="Str">SingleChannel.exe</Property>
            <Property Name="FileInfo[0].FileTag" Type="Str">{2CC57508-47FA-4C26-9E37-26ED9CF7444B}</Property>
            <Property Name="FileInfo[0].Type" Type="Int">3</Property>
            <Property Name="FileInfo[0].TypeID" Type="Ref">/My Computer/Build Specifications/Dutta Single-Channel Acq</Property>
            <Property Name="FileInfo[1].DirTag" Type="Str">{8323CD25-0B33-4728-8A03-A9E27956561C}</Property>
            <Property Name="FileInfo[1].FileName" Type="Str">SingleChannel.aliases</Property>
            <Property Name="FileInfo[1].FileTag" Type="Str">{9837BAA6-5738-41C5-BEE0-719CCA480490}</Property>
            <Property Name="FileInfo[1].Type" Type="Int">3</Property>
            <Property Name="FileInfo[1].TypeID" Type="Ref">/My Computer/Build Specifications/Dutta Single-Channel Acq</Property>
            <Property Name="FileInfo[2].DirTag" Type="Str">{8323CD25-0B33-4728-8A03-A9E27956561C}</Property>
            <Property Name="FileInfo[2].FileName" Type="Str">SingleChannel.ini</Property>
            <Property Name="FileInfo[2].FileTag" Type="Str">{B3AA75AB-4292-4323-B85A-ADD20467D61F}</Property>
            <Property Name="FileInfo[2].Type" Type="Int">3</Property>
            <Property Name="FileInfo[2].TypeID" Type="Ref">/My Computer/Build Specifications/Dutta Single-Channel Acq</Property>
            <Property Name="InstSpecVersion" Type="Str">8218002</Property>
            <Property Name="LicenseFile" Type="Ref"></Property>
            <Property Name="OSCheck" Type="Int">0</Property>
            <Property Name="OSCheck_Vista" Type="Bool">false</Property>
            <Property Name="ProductName" Type="Str">dutta.pci-6251</Property>
            <Property Name="ProductVersion" Type="Str">1.0.2</Property>
            <Property Name="ReadmeFile" Type="Ref"></Property>
            <Property Name="ShortcutInfo.Count" Type="Int">1</Property>
            <Property Name="ShortcutInfo[0].DirTag" Type="Str">{B9E310F1-839C-48B7-8CAE-33000780C26E}</Property>
            <Property Name="ShortcutInfo[0].FileTag" Type="Str">{2CC57508-47FA-4C26-9E37-26ED9CF7444B}</Property>
            <Property Name="ShortcutInfo[0].FileTagDir" Type="Str">{8323CD25-0B33-4728-8A03-A9E27956561C}</Property>
            <Property Name="ShortcutInfo[0].Name" Type="Str">SingleChannel</Property>
            <Property Name="ShortcutInfo[0].SubDir" Type="Str">dutta.pci-6251</Property>
            <Property Name="UpgradeCode" Type="Str">{C701E5C2-3265-4D3B-A0E9-1ACC40E9042C}</Property>
            <Property Name="WindowMessage" Type="Str">This will install a simple application to acquire voltage data from the NI PCI-6251. </Property>
            <Property Name="WindowTitle" Type="Str">Single-Channel Acquisition</Property>
         </Item>
      </Item>
   </Item>
</Project>
